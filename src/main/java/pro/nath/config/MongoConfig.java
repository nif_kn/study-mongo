package pro.nath.config;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.Arrays;

/**
 * Created by nkozhevnikova on 27.09.2016.
 * Класс является конфигурацией для mongo
 */

@Configuration
@EnableMongoRepositories(basePackages = "db")
public class MongoConfig extends AbstractMongoConfiguration {

    @Autowired
    private Environment env;

 /*   @Bean
    public MongoClientFactoryBean mongo() {
        MongoClientFactoryBean mongo = new MongoClientFactoryBean();
        mongo.setHost("localhost");
        return mongo;
    }
    @Bean
    public MongoOperations mongoTemplate(Mongo mongo) {
        return new MongoTemplate(mongo, "studydb");
    }*/

    /**
     *  Bean'ы уже включены в AbstractMongoConfiguration
     */

    @Override
    protected String getDatabaseName() {
        return "studydb";
    }
    @Override
    public Mongo mongo() throws Exception {
        MongoCredential credential =
                MongoCredential.createMongoCRCredential(
                        env.getProperty("mongo.username"),
                        "studydb",
                        env.getProperty("mongo.password").toCharArray());
        return new MongoClient(
                new ServerAddress("localhost", 27017),
                Arrays.asList(credential));
    }
}

