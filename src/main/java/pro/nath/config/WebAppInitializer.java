package pro.nath.config;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

/** Данный класс служит для определения конфигурации сервлета
 *
 */
public class WebAppInitializer implements WebApplicationInitializer {

    /**
     * Реализация метода onStartup
     *  Определяется контекст приложения
     *  регистрируются классы конфигурации {@link MongoConfig}
     *  Создается dispatcher сервлет {@link DispatcherServlet}
     */

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.register(MongoConfig.class);
        context.register(WebConfig.class);

        servletContext.addListener(new ContextLoaderListener(context));
        ServletRegistration.Dynamic servlet = servletContext.addServlet("dispatcher",new DispatcherServlet(context));
        servlet.addMapping("/");
        servlet.setLoadOnStartup(0);

    }
}
