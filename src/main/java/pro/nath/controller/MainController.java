package pro.nath.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import pro.nath.service.OrderService;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Данный класс помечается аннотацией Controller
 * является контроллером в Spring MVC
 */
@Controller
public class MainController {
    @Autowired
    OrderService orderService;

    public void test() {
        orderService.findByClient("Natasha");
    }

    public static void main(String[] args) {
        int a = 1 + 2;
    }

    @RequestMapping(value = "/", method = GET)
    public String index() {
        return "index";
    }
}
