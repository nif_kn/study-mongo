package pro.nath.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import pro.nath.entity.Order;

import java.util.List;

/**
 * Класс осуществляет манипуляции данными Order в БД
 */

public class OrderService {

    @Autowired
    MongoOperations mongo;


    public long getOrderCount() {
        return mongo.getCollection("order").count();
    }

    public void saveOrder(Order order) {
        mongo.save(order, "order");
    }

    public Order findById(String id) {
        return mongo.findById(id, Order.class);
    }

    public List<Order> findByClient(String clientName) {
        return mongo.find(Query.query(Criteria.where("customer").is(clientName)), Order.class);
    }

    public List<Order> findByClient(String clientName, String clientType) {
        return mongo.find(Query.query(Criteria.where("customer").is(clientName).and("type").is(clientType)), Order.class);
    }

    public void remove(Order order) {
        mongo.remove(order);
    }
}
