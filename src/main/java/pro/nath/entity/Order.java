package pro.nath.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Collection;
import java.util.LinkedHashSet;

/**
 * Класс описывает документ Order в коллекции studydb
 */

@Document
public class Order {

    @Id
    private String id;

    /**
     * Когда документ сохраняется, то настройка customer
     * мапится в поле с именем client
     */

    @Field("client")
    private String customer;

    //все объекты Java будут сохраняться как поля документа
    private String type;
    private Collection<Item> items = new LinkedHashSet<>();

    public String getCustomer() {
        return customer;
    }
    public void setCustomer(String customer) {
        this.customer = customer;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public Collection<Item> getItems() {
        return items;
    }
    public void setItems(Collection<Item> items) {
        this.items = items;
    }
    public String getId() {
        return id;
    }
}
